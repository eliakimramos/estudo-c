﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lista = new string[5];
            lista[0] = "Eliakim";
            lista[1] = "Ramos";
            lista[2] = "de";
            lista[3] = "Souza";
            lista[4] = "    ";

            for (int i=0; i < 5; i++)
            {
                Console.Write(lista[i]);
                Console.Write(" ");
            }
            Console.ReadKey();
        }
    }
}

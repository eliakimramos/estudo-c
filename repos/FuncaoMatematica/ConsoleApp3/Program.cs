﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            double numero1;
            double numero2;
            double Total;
            string operacao;
            string operacaoNome;
            Console.WriteLine("Digite um numero:");
            numero1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Digite a operação (+, -, *, /):");
            operacao = Console.ReadLine();
            Console.WriteLine("Digite outro numero:");
            numero2 = Convert.ToDouble(Console.ReadLine());
            if(operacao == "+")
            {
                Total = numero1 + numero2;
                operacaoNome = "Soma";

            }else if( operacao == "-")
            {
                Total = numero1 - numero2;
                operacaoNome = "Subtração";

            }else if(operacao == "*")
            {
                Total = numero1 * numero2;
                operacaoNome = "Mutiplicação";

            }else if(operacao == "/")
            {
                Total = numero1 / numero2;
                operacaoNome = "Divisão";
            }
            else
            {
                Total = 0;
                operacaoNome = "operação digitada não é valida";
                Console.WriteLine(operacaoNome);
                Console.ReadKey();
                Environment.Exit(1);
            }
            Console.WriteLine(" A "+ operacaoNome + " de " + numero1 + " e " + numero2 + " é " + Total);
            Console.ReadKey();
            Console.Clear();

            //uso do switch

            Console.WriteLine("Digite um numero:");
            numero1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Digite a operação (+, -, *, /):");
            operacao = Console.ReadLine();
            Console.WriteLine("Digite outro numero:");
            numero2 = Convert.ToDouble(Console.ReadLine());

            switch (operacao)
            {
                case "+":
                        Total = numero1 + numero2;
                        operacaoNome = "Soma";
                break;
                case "-":
                        Total = numero1 - numero2;
                        operacaoNome = "Subtração";
                break;
                case "*":
                        Total = numero1 * numero2;
                        operacaoNome = "Mutiplicação";
                break;
                case "/":
                        Total = numero1 / numero2;
                        operacaoNome = "Divisão";
                break;
                default:
                    Total = 0;
                    operacaoNome = "operação digitada não é valida";
                    Console.WriteLine(operacaoNome);
                    Console.ReadKey();
                    Environment.Exit(1);
                break;
            }

            Console.WriteLine(" A " + operacaoNome + " de " + numero1 + " e " + numero2 + " é " + Total);
            Console.ReadKey();
        }
    }
}

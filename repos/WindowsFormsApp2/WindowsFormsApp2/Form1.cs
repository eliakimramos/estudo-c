﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        double total        = 0;
        double ultimoNumero = 0;
        string operacao     = "";

        public Form1()
        {
            InitializeComponent();
        }
        private void Limpar()
        {
            total              = 0;
            ultimoNumero       = 0;
            operacao           = "";
            txtResultado.Text  = "";
        }

        private void Calcular()
        {
            switch (operacao)
            {
                case "+":
                    total += ultimoNumero;
                    break;
                case "-":
                    if (total == 0 )
                    {
                        total = ultimoNumero - total;
                    }
                    else
                    {
                        total -= ultimoNumero;
                    }
                    break;
                case "*":
                    if(total == 0 )
                    {
                        total = ultimoNumero * 1;
                    }
                    else
                    {
                        total *= ultimoNumero;
                    }
                    
                    break;
                case "/":
                    if (total == 0 )
                    {
                        total = ultimoNumero / 1;
                    }
                    else
                    {
                        total /= ultimoNumero;
                    }
                    break;
            }
            txtResultado.Text = total.ToString();
            ultimoNumero = 0;
        }

        private void addNumeros(string valor)
        {
            if(operacao == "" && total == 0 || total != Convert.ToDouble(txtResultado.Text))
            {
                txtResultado.Text += valor;
            }
            else if(total == Convert.ToDouble(txtResultado.Text))
            {
                txtResultado.Text = "";
                txtResultado.Text += valor;
            }
            
            ultimoNumero = Convert.ToDouble(txtResultado.Text);

        }
        private void button6_Click(object sender, EventArgs e)
        {
            addNumeros("6");
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            addNumeros("0");
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            addNumeros("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            addNumeros("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            addNumeros("3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            addNumeros("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            addNumeros("5");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            addNumeros("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            addNumeros("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            addNumeros("9");
        }

        private void btnMais_Click(object sender, EventArgs e)
        {
           if(operacao == "")
            {
                operacao = "+";
                Calcular();
            }
            else
            {
                Calcular();
                operacao = "+";
            }

        }

        private void btnMenos_Click(object sender, EventArgs e)
        {
            if (operacao == "")
            {
                operacao = "-";
                Calcular();
            }
            else
            {
                Calcular();
                operacao = "-";
            }
        }

        private void btnVezes_Click(object sender, EventArgs e)
        {
            if (operacao == "")
            {
                operacao = "*";
                Calcular();
            }
            else
            {
                Calcular();
                operacao = "*";
            }

        }

        private void btnDividir_Click(object sender, EventArgs e)
        {
            if (operacao == "")
            {
                operacao = "/";
                Calcular();
            }
            else
            {
                Calcular();
                operacao = "/";
            }

        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            ultimoNumero = Convert.ToDouble(txtResultado.Text);
            Calcular();
        }
    }
}

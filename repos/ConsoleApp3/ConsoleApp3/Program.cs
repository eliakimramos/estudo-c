﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            double n1 = 0;
            double t = 0;
            string op = "+";

            while( op != "=")
            {
                Console.Write("Entre com o Valor : ");
                n1 = Convert.ToDouble(Console.ReadLine());
                switch (op)
                {
                    case "+":
                        t += n1;
                        break;
                    case "-":
                        t -= n1;
                        break;
                    case "*":
                        t = t * n1;
                        break;
                    case "/":
                        t = t / n1;
                        break;
                }

                Console.WriteLine("Total da operação no momento : "+t);
                Console.Write("Informe a proxima operação ( + , - , * , / ) : ");
                op = Console.ReadLine();
            }

            Console.Write("O resultado de todos os calculos é : " + t);
            Console.ReadKey();

        }
    }
}
